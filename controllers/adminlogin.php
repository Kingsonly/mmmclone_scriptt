<?php

class adminlogin extends Controller {

	function __construct() {

		parent::__construct();
		
	}
	
	function index() 
	{	
		$message ='';
		if(isset($_POST['username'])){
		$message = $this->run();
		}

		$this->view->render('login/adminlogin', $noinclude=true, $message);

		
	}
	
	function run()
	{
		return $this->model->adminlogin($_POST['username'], $_POST['password']);

	}
	

}