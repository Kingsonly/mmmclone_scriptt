<?php

class api extends Controller {

	function __construct() {
	//echo 'reach here';	
	}
	
	function consultant() 
	{	
		include("./models/admindashboard_model.php");
		$admindashboard = new admindashboard_Model;
		$this->model=$admindashboard->getconsultants($json=true);
		
	
	}
	
	function properties() 
	{	
		include("./models/admindashboard_model.php");
		$admindashboard = new admindashboard_Model;
		$this->model=$admindashboard->getproperties($json=true);
		
	
	}
	
	function viewproperty() 
	{	
	$message=$this->view->message='';

		include("./models/admindashboard_model.php");
		$admindashboard = new admindashboard_Model;
		$this->model=$admindashboard->viewproperty($json=true);
		
	}

	
	function transactions() 
	{	
		include("./models/admindashboard_model.php");
		$admindashboard = new admindashboard_Model;
		$this->model=$admindashboard->gettransactions($json=true);
		
	
	}
	
	function commissions() 
	{	
		include("./models/admindashboard_model.php");
		$admindashboard = new admindashboard_Model;
		$this->model=$admindashboard->getcommissions($json=true);
		
	
	}
	
	function registerconsultant(){
		
	include("./models/admindashboard_model.php");
	$admindashboard = new admindashboard_Model;
	$this->model=$admindashboard->registerconsultant($json=true);
			
		
	}


	function generation(){
		
	include("./models/admindashboard_model.php");
	$admindashboard = new admindashboard_Model;
	$this->model=$admindashboard->generation($json=true);
			
		
	}


	
	function registerclient(){
		
	include("./models/admindashboard_model.php");
	$admindashboard = new admindashboard_Model;
	$this->model=$admindashboard->registerclient($json=true);
			
		
	}
	
	function bulksms(){
	include("./models/admindashboard_model.php");
	$admindashboard = new admindashboard_Model;
	$this->model=$admindashboard->bulksms();
	
	}
	
}