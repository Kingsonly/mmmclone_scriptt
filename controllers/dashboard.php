<?php

class Dashboard extends Controller {
  
        
	//public $data=array();
	public $loggedid;
	public $water;
	function __construct() {
		parent::__construct();
		Session::init();
		$logged = Session::get('loggedIn');
        
		$this->view->data['user_details']=Session::get('user_details');

		$loggedid = Session::get('user_id');
 
		if ($logged == false) {
			Session::destroy();
			header('location: ./login');
			exit;
		}
	
		$this->view->js = array('dashboard/js/default.js'); 
		
	}  
	
	function index() 
	{	
        $user_status=$this->model->users_status_info(Session::get('user_id'));
        
        
		
        if($user_status[0]['status']==0 or $user_status[0]['status']==5 ){
            $this->view->data['get_ph']=$this->model->get_ph_info(Session::get('user_id'));
            $this->view->data['get_ph1']='Give help';
            
        }else{
            $this->view->data['get_gh']=$this->model->get_gh_info(Session::get('user_id'));
            $this->view->data['get_gh1']="get help";
        }
        
        $this->view->data['gh_count']=$this->model->get_total_gh_ph(Session::get('user_id'),'gh_id') ;
            $this->view->data['ph_count']=$this->model->get_total_gh_ph(Session::get('user_id'),'ph_id') ;
        $this->view->data['testimonials']=$this->model->view_testimonila();
       
        $this->view->render('dashboard/consultant/index',$noinclude=false,2); 
		//$this->data['id']=$this->loggedid;

		
	}
    
    function logout()
	{
		Session::destroy();
		header('location:'.URL.'login'); 
		exit;
       // print_r(123);
	}
    
    function confirms(){
        $getgh=$_GET['ghb'];
        $getph=$_GET['phb'];
        if(isset($getgh)){
            $xpldvalue=explode('~',$getgh);
            $id=$xpldvalue[0];
            $status1=$this->model->users_status_info($id) ;
            $status=$status1[0]['status']+2;
            $id1=$xpldvalue[2];
            $phid=$xpldvalue[1];
            $this->model->update_status1($id,$status);
            $this->model->update_status2($id1,1,'gh_status');
            $this->model->update_status2($id1,1,'status');
            $this->model->update_status1($phid,1);
            
            echo $status;
        }elseif(isset($getph)){
            $xpldvalue=explode('~',$getph);
            $id=$xpldvalue[0];
            
            
            $id1=$xpldvalue[2];
            
            $this->model->update_status2($id1,1,'ph_status');
            
            
            echo $status;
        }
        
    }
    
    
    function profile() 
	{	
        
        
        if(isset($_FILES["file"]["type"]))
        {
            $validextensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $_FILES["file"]["name"]);
            $file_extension = end($temporary);
            if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
            ) && ($_FILES["file"]["size"] < 100000)//Approx. 100kb files can be uploaded.
            && in_array($file_extension, $validextensions)) {
            if ($_FILES["file"]["error"] > 0)
            {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
            }
            else
            {
            if (file_exists("upload/" . $_FILES["file"]["name"])) {
            echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
            }
            else
            {
            $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
            $targetPath = "upload/".$_FILES['file']['name']; // Target path where file is to be stored
            move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
            echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
            echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
            echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
            echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
            echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";
            }
            }
            }
            else
            {
            echo "<span id='invalid'>***Invalid file Size or Type***<span>";
            }
        }

                
            $this->view->data['gh_count']=$this->model->get_total_gh_ph(Session::get('user_id'),'gh_id') ;
            $this->view->data['ph_count']=$this->model->get_total_gh_ph(Session::get('user_id'),'ph_id') ;
           // $this->view->data['commision'];//$this->model->get_all_tbl_commissions_numrows(Session::get('user_id'));
            $this->view->data['session']=Session::get('loggedIn');
            
            $this->view->render('dashboard/consultant/profile',$noinclude=false,2);
               
	}
    
    
    function complains(){
        $this->view->render('dashboard/consultant/complains',$noinclude=false,2);
        
    }
    
     function disableaccount(){
        $this->view->render('dashboard/consultant/disableaccount',$noinclude=false,2);
        
    }
    
    function extention(){
        $this->view->render('dashboard/consultant/extention',$noinclude=false,2);
        
    }
    
    function testimony(){
        $this->view->render('dashboard/consultant/testimony',$noinclude=false,2);
        
    }
    
    function runcomplain(){
        $subject=$_POST['subject'];
        $content=$_POST['content'];
        $id=$_POST['id'];
        $status=$_POST['status'];
        if(isset($subject)){
        $this->model->insert_complain($id,$subject,$content,$status);
            }
    }
    
    function runtestimonials(){
         $pix=$_POST['pix'];
        $content=$_POST['content'];
        $id=$_POST['id'];
        $name=$_POST['name'];
        if(isset($content)){
        $this->model->insert_testimonila($id,$name,$pix,$content);
        }
    }
    
/*
	function downline() 
	{	//$rat=$this->water="whater";
		//$this->view->dat=Session::get('user_id');

		//$this->view->data['session_details']=Session::get('user_id');
        Session::set('count', $this->model->get_all_downline_numrow(Session::get('user_id')));
	$this->view->data['downline']=$this->model->get_all_downline(Session::get('user_id'));
        
	$this->view->data["logged"]=Session::get('loggedIn');
	$this->view->data["user_id"]=Session::get('user_id');
		$this->view->render('dashboard/consultant/downline',$noinclude=false,1);
		//$this->data['id']=$this->loggedid;
	}
    
    function downline_sec() 
	{	//$rat=$this->water="whater";
		//$this->view->dat=Session::get('user_id');

		//$this->view->data['session_details']=Session::get('user_id');
	$this->view->data['downline']=$this->model->get_all_downline(Session::get('user_id'));
	$this->view->data["logged"]=Session::get('loggedIn');
	$this->view->data["user_id"]=Session::get('user_id');
		$this->view->render('dashboard/consultant/second_gen',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    function downline_third() 
	{	//$rat=$this->water="whater";
		//$this->view->dat=Session::get('user_id');

		//$this->view->data['session_details']=Session::get('user_id');
	$this->view->data['downline']=$this->model->get_all_downline(Session::get('user_id'));
	$this->view->data["logged"]=Session::get('loggedIn');
	$this->view->data["user_id"]=Session::get('user_id');
		$this->view->render('dashboard/consultant/third_gen',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    function downline_forth() 
	{	//$rat=$this->water="whater";
		//$this->view->dat=Session::get('user_id');

		//$this->view->data['session_details']=Session::get('user_id');
	$this->view->data['downline']=$this->model->get_all_downline(Session::get('user_id'));
	$this->view->data["logged"]=Session::get('loggedIn');
	$this->view->data["user_id"]=Session::get('user_id');
		$this->view->render('dashboard/consultant/forth_gen',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    function downline_five() 
	{	//$rat=$this->water="whater";
		//$this->view->dat=Session::get('user_id');

		//$this->view->data['session_details']=Session::get('user_id');
	$this->view->data['downline']=$this->model->get_all_downline(Session::get('user_id'));
	$this->view->data["logged"]=Session::get('loggedIn');
	$this->view->data["user_id"]=Session::get('user_id');
		$this->view->render('dashboard/consultant/fifth_gen',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}



	function downline_no_header() 
	{	//$rat=$this->water="whater";
		//$this->view->dat=Session::get('user_id');

		//$this->view->data['session_details']=Session::get('user_id');
	$this->view->data['downline']=$this->model->get_all_downline(Session::get('user_id'));
	$this->view->data["logged"]=Session::get('loggedIn');
	$this->view->data["user_id"]=Session::get('user_id');
		//$this->view->render('dashboard/consultant/downline',$noinclude=true,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
	
    
    function properties() 
	{	
	$this->view->data['properties']=$this->model->get_all_properties();
	
		$this->view->render('dashboard/consultant/properties',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    
    function properties_no_header() 
	{	
	$this->view->data['properties']=$this->model->get_all_properties();
	
		
	}
    
    
    function transactions() 
	{	
	$this->view->data['transactions']=$this->model->get_all_tbl_transactions_consultants(Session::get('user_id'));
	
	
		$this->view->render('dashboard/consultant/transactions',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    function transactions_no_header() 
	{	
	$this->view->data['transactions']=$this->model->get_all_tbl_transactions_consultants(Session::get('user_id'));
	
	}
    
    function sms(){
        $this->view->data['transactions']=$this->model->get_all_tbl_transactions_consultants(Session::get('user_id'));
	
	
		$this->view->render('dashboard/consultant/sms',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
        
    }
    
    function smssend(){
        //$this->view->data['transactions']=$this->model->get_all_tbl_transactions_consultants(Session::get('user_id'));
	
	
		//$this->view->render('dashboard/consultant/sms',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
        //echo $_POST['message'];
        
        $owneremail="kingsonly13c@gmail.com";
$subacct="KINGSONLY";
$subacctpwd="firstoctober";
$sendto="08153259099";
$sender="PHP DEMO";
$message="This is a test";
/* create the required URL */
/* destination number */
/* sender id */
/* message to be sent */
    /*
$url =
"http://www.smslive247.com/http/index.aspx?" . "cmd=sendquickmsg"
. "&owneremail=" . UrlEncode($owneremail)
. "&subacct=" . UrlEncode($subacct)
. "&subacctpwd=" . UrlEncode($subacctpwd) . "&message=" . UrlEncode("1234")."&sendto=".UrlEncode("08153259099")."&sender=".UrlEncode($sender);
/* call the URL */


/*
if ($f = @fopen($url, "r"))
{
      $answer = fgets($f, 255);
      if (substr($answer, 0, 1) == "+")
      {
            echo "SMS to $dnr was successful.";
}
    elseif($answer){
        echo 'message sent';
    }
else {
echo "an error has occurred: [$answer].";
} }

else{
echo "url could no be oppened";
}
        
    }
    
    
 function commissions() 
	{	
	$this->view->data['commission']=$this->model->get_commision_for_consultant(Session::get('user_id'));
	
	
		$this->view->render('dashboard/consultant/commission',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
	
	function logout()
	{
		Session::destroy();
		header('location:'.URL.'login');
		exit;
	}
	
    
    function profile() 
	{	
        
        
        if(isset($_FILES["file"]["type"]))
{
$validextensions = array("jpeg", "jpg", "png");
$temporary = explode(".", $_FILES["file"]["name"]);
$file_extension = end($temporary);
if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
) && ($_FILES["file"]["size"] < 100000)//Approx. 100kb files can be uploaded.
&& in_array($file_extension, $validextensions)) {
if ($_FILES["file"]["error"] > 0)
{
echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
}
else
{
if (file_exists("upload/" . $_FILES["file"]["name"])) {
echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
}
else
{
$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
$targetPath = "upload/".$_FILES['file']['name']; // Target path where file is to be stored
move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";
}
}
}
else
{
echo "<span id='invalid'>***Invalid file Size or Type***<span>";
}
}
        
        //$rat=$this->water="whater";
		//$this->view->dat=Session::get('user_id');

		//$this->view->data['session_details']=Session::get('user_id');
        //Session::set('count', $this->model->get_all_downline_numrow(Session::get('user_id')));
	$this->view->data['clientcount']=$this->model->get_all_tbl_clients_numrows(Session::get('user_id'));
       $this->view->data['commision']=$this->model->get_all_tbl_commissions_numrows(Session::get('user_id'));
	//$this->view->data["logged"]=Session::get('loggedIn');
	//$this->view->data["user_id"]=Session::get('user_id');
		$this->view->render('dashboard/consultant/profile',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    
    function email() 
	{	//$rat=$this->water="whater";
		//$this->view->dat=Session::get('user_id');

		//$this->view->data['session_details']=Session::get('user_id');
        //Session::set('count', $this->model->get_all_downline_numrow(Session::get('user_id')));
	//$this->view->data['downline']=$this->model->get_all_downline(Session::get('user_id'));
        
	//$this->view->data["logged"]=Session::get('loggedIn');
	//$this->view->data["user_id"]=Session::get('user_id');
		$this->view->render('dashboard/consultant/email',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    
    function clients() 
	{	
	$this->view->data['client']=$this->model->get_all_tbl_clients(Session::get('user_id'));
	
	
		$this->view->render('dashboard/consultant/client',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    
    
    
    function cofo() 
	{	
	$this->view->data['client']=$this->model->get_all_tbl_clients(Session::get('user_id'));
	
	
		$this->view->render('dashboard/consultant/cofo',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    
    function smsselect() 
	{	$gets=$_GET['selects_property'];
        if(isset($gets)){
            $this->view->data['property_details']=$this->model->get_specific_properties($gets);
            
        }
	
	
	
		$this->view->render('dashboard/consultant/sms_select',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}
    
    function downline_select() 
	{	$downline_select=$_GET['downline_select'];
        if(isset($downline_select)){
           $this->view->data['downline_select']=$this->model->get_all_downline($downline_select); 
        }
	
		$this->view->render('dashboard/consultant/downline_select',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}

    
    function invoice() 
	{	$invoices=$_GET['invoice'];
        if(isset($invoices)){
           $this->view->data['invoice']=$this->model->get_all_tbl_transactions_consultants_id($invoices); 
        }
	
		$this->view->render('dashboard/consultant/invoice',$noinclude=false,0,Session::get('loggedIn'));
		//$this->data['id']=$this->loggedid;
	}

    
    function profile_run() 
	{	
        if(isset($_GET['upload'])){
           if(is_array($_FILES)) {
if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
$sourcePath = $_FILES['userImage']['tmp_name'];
$targetPath = "public/images/".$_FILES['userImage']['name'];
if(move_uploaded_file($sourcePath,$targetPath)) {
    $this->model->update_into(Session::get('user_id'),'tbl_consultant','pic_path',$targetPath);
?>

<img src="<?php echo URL.$targetPath; ?>" width="100px" height="100px" />
<?php
}
}
}else{ ?>
            echo "Pls select a proper picture";
<?php
        }
        }
        
        
        if(isset($_GET['newpassword'])){
            if(!isset($_POST['password'])==''){
                //insert into db
                $pwd=md5($_POST['password']);
                $this->model->update_into(Session::get('user_id'),'tbl_consultant','password',$pwd);
                echo 'password updated';
            }else{
                echo 'sorry  field cant be left empty';
            }
            
            
        }
	
        if(isset($_GET['newadress'])){
            if(!isset($_POST['address'])==''){
                //insert into db
               $address=$_POST['address'];
                $this->model->update_into(Session::get('user_id'),'tbl_consultant','address',$address);
                
            }else{
                echo 'sorry  field cant be left empty';
            }
            
            
        }
	
	}
*/

}