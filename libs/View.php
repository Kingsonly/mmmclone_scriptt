<?php

class View {
public $data = array();
	function __construct() {
		//echo 'this is the view';
		//$this->view->logged = Session::get('loggedIn');
		//$this->view->loggedType = Session::get('loggedType');

	}

	public function render($name, $noInclude, $message)
	{
		if ($noInclude == true) {

			extract($this->data);
			require 'views/snipets/headerref.php';
			require 'views/' . $name . '.php';
			require 'views/snipets/footerref.php';		
		}
		else {
			extract($this->data);
			
			//var_dump($this->view->loggedType);
			if($message==1){
                require 'views/snipets/headerref.php';
				require 'views/common/header.php';
			}if($message==2){
                require 'views/snipets/headerref_dashboard.php';
                require 'views/common/header_client.php';
            }if($message==3){
                require 'views/snipets/headerref_dashboard.php';
                require 'views/common/header_admin.php';
            }
			require 'views/' . $name . '.php';
            if($message==1){
               require 'views/common/footer.php';	
			   require 'views/snipets/footerref.php';	
            }if($message==2){
               require 'views/common/footer_client.php';	
			   require 'views/snipets/footerref_dashboard.php';
            }if($message==3){
                require 'views/common/footer_admin.php';	
			    require 'views/snipets/footerref_dashboard.php';		
            }
			
		}
	}



}


