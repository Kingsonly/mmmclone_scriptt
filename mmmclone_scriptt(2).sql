-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 12, 2017 at 02:51 PM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mmmclone_scriptt`
--

-- --------------------------------------------------------

--
-- Table structure for table `complains`
--

CREATE TABLE IF NOT EXISTS `complains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `usersname` varchar(233) NOT NULL,
  `message` varchar(233) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `kind` int(11) NOT NULL,
  `subject` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `complains`
--

INSERT INTO `complains` (`id`, `userid`, `usersname`, `message`, `status`, `kind`, `subject`) VALUES
(1, 0, '', '                      \n                    test', 0, 1, 'test'),
(2, 47, '', '                      \n                    test', 0, 1, 'test'),
(3, 47, '', '                      \n                    was busy', 0, 2, 'was busy'),
(4, 47, '', '                      \n                    jfjfjfjfjfjfjfj', 0, 1, 'fjfjfjfjfjf'),
(5, 47, '', '                      \n                    111111', 0, 1, '1111'),
(6, 47, '', '                      \n                    111111', 0, 1, '1111'),
(7, 47, '', '                      \n                    111111', 0, 1, '1111'),
(8, 47, '', '                      \n                    111111', 0, 1, '1111'),
(9, 47, '', '                      \n                    111111', 0, 1, '1111'),
(10, 47, '', '                      \n                    qqqqqq', 0, 1, 'qqqqq'),
(11, 47, '', '                      \n                    qqqqqq', 0, 1, 'qqqqq'),
(12, 47, '', '                      \n                    qqqqqq', 0, 1, 'qqqqq'),
(13, 47, '', '                      \n                    eeeee', 0, 1, 'eeee'),
(14, 47, '', '                      \n                    dddddd', 0, 1, 'dddd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gh_ph`
--

CREATE TABLE IF NOT EXISTS `tbl_gh_ph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gh_id` int(11) NOT NULL,
  `ph_id` int(11) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `plan` int(11) NOT NULL,
  `gh_status` int(11) NOT NULL DEFAULT '0',
  `ph_status` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tbl_gh_ph`
--

INSERT INTO `tbl_gh_ph` (`id`, `gh_id`, `ph_id`, `start_time`, `end_time`, `plan`, `gh_status`, `ph_status`, `status`) VALUES
(1, 47, 46, 'Mar-24-2017 15:03:22', 'Mar-24-2017 22:03:22', 50, 1, 1, 1),
(10, 47, 264, 'Mar-26-2017 23:03:00', 'Mar-27-2017 23:03:00', 50, 1, 1, 1),
(11, 264, 46, 'Mar-28-2017 00:03:00', 'Mar-29-2017 00:03:00', 50, 1, 0, 1),
(12, 47, 265, 'Apr-01-2017 15:04:00', 'Apr-02-2017 15:04:00', 50, 1, 0, 1),
(13, 264, 47, 'Apr-01-2017 16:04:00', 'Apr-06-2017 16:04:00', 50, 1, 1, 1),
(14, 47, 264, 'Apr-01-2017 16:04:00', 'Apr-06-2017 16:04:00', 50, 1, 0, 1),
(15, 47, 268, 'Apr-08-2017 10:04:00', 'Apr-13-2017 10:04:00', 50, 1, 0, 1),
(16, 46, 269, 'Apr-08-2017 10:04:01', 'Apr-13-2017 10:04:01', 50, 0, 0, 0),
(17, 264, 47, 'Apr-08-2017 11:04:00', 'Apr-13-2017 11:04:00', 50, 0, 0, 0),
(18, 47, 46, 'Apr-08-2017 16:04:02', '2017/04/13', 50, 1, 0, 1),
(19, 265, 264, 'Apr-08-2017 16:04:02', '2017/04/13', 50, 0, 0, 0),
(20, 47, 272, 'Apr-08-2017 23:04:00', 'Apr-13-2017 23:04:00', 50, 1, 0, 1),
(21, 46, 273, 'Apr-09-2017 21:04:00', '2017/04/14', 50, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_memberslogin`
--

CREATE TABLE IF NOT EXISTS `tbl_memberslogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `gender` int(11) NOT NULL,
  `pic_path` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `ref` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `plan` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_number` int(11) NOT NULL,
  `deactivate` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=275 ;

--
-- Dumping data for table `tbl_memberslogin`
--

INSERT INTO `tbl_memberslogin` (`id`, `email`, `password`, `firstname`, `lastname`, `gender`, `pic_path`, `dob`, `state`, `country`, `phone`, `ref`, `status`, `plan`, `bank_name`, `account_name`, `account_number`, `deactivate`) VALUES
(47, 'kingsonly13c@gmail.com', 'fe01ce2a7fbac8fafaed7c982a04e229', 'kingsonly13c@gmail.com', 'kings', 2, '', '10/10/10', 'abuja', 'nigeria', '2147483647', 81657, 10, 50, 'fidelity', 'achumie kingsley', 101010, 0),
(46, 'kingsonly@gmail.com', 'fe01ce2a7fbac8fafaed7c982a04e229', 'achumie', 'kingsley', 1, '', '16/10/60', 'abuja', 'nigeria', '08153259099', 16066, 2, 50, 'fidelity', 'achumie kingsley', 101010, 0),
(264, 'kingsonly13kc@gmail.com', 'fe01ce2a7fbac8fafaed7c982a04e229', 'mike', 'john', 1, '', '2017-03-14', 'abuja', 'nigeria', '2147483647', 48260, 5, 50, 'fidelity', 'achumie kingsley', 101010, 0),
(265, '111@nnn.com', '2d02e669731cbade6a64b58d602cf2a4', '1111', '1111', 1, '', 'sssss', 'sssss', 'sssss', 'sssss', 19196, 2, 50, 'sssssss', 'ssssss', 0, 0),
(266, 'kingsonly', 'fe01ce2a7fbac8fafaed7c982a04e229', 'jgfgrgf', 'orhuerhuiorhur', 2, '', '1', 'anambra', 'nigeria@gmail.com', '08153259099', 47006, 0, 0, '', '', 0, 0),
(267, 'kkkkkkk', 'd8e71dbd1afc289a4b102eeadeb6f363', 'kkkkk', 'kkkkkk', 1, '', '1', 'kkkkkk', 'kkkkkkk@hhh.jjjj', 'kkkkkkk', 91129, 0, 200, '', '', 0, 0),
(268, 'sfghj', 'a152e841783914146e4bcd4f39100686', 'rtyuio', 'wertyhjkl', 1, '', '1', 'qwertyui', 'dfghjk@gmail.com', '0900009090', 68461, 1, 50, '', '', 0, 0),
(269, 'kjhgf', '794f91d108aa95f0587c0e7a84132407', 'rrrrrr', 'kjhgfd', 1, '', '1', 'oiuyhgfd', 'oiuytr@ggg.hhh', 'iuyt', 28866, 5, 50, '', '', 0, 0),
(270, 'gggggggg', '9c72446df124ddf214b698c1e2312371', 'ddddddd', 'fffffffff', 1, '', '1', 'hyyyyyyy', 'hhhhhhhh@mmm.mmm', 'ggggggggg', 53942, 0, 200, '', '', 0, 0),
(271, 'ytytytyty', 'ac94dd0c8534302678183a35d80fb096', 'uuuuu', 'ttytytytyty', 2, '', '1', 'jfjjgjgjg', 'jgjgjgj@gmail.com', 'ytytytyty ', 25295, 0, 200, '', '', 0, 0),
(272, 'dddd', '11ddbaf3386aea1f2974eee984542152', 'dddd', 'ddddd', 1, '', '1', 'dddd', 'dddd', 'dddd', 42430, 1, 50, '', '', 0, 0),
(273, 'kkkkkkkk', 'd8e71dbd1afc289a4b102eeadeb6f363', 'yyyyyy', 'kkkkkkkk', 1, '', '1', 'kkkkkkk', 'kkkkkkkkk', 'kkkkkk', 74981, 5, 50, 'kkkkkkk', 'kkkkkkk', 0, 0),
(274, 'qqqqq', '437599f1ea3514f8969f161a6606ce18', 'qqqq', 'qqqq', 1, '', '1', 'qqqqqqqqq', 'qqqqqqq', 'qqqqqq', 77851, 0, 200, 'qqqqqqqqq', 'qqqqq', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_id` int(11) NOT NULL,
  `mem_name` varchar(11) NOT NULL,
  `mem_pix` varchar(222) NOT NULL,
  `mem_comment` varchar(222) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `mem_id`, `mem_name`, `mem_pix`, `mem_comment`, `status`) VALUES
(1, 123, 'kingsonly a', '', 'fjfjfj fhfhfhfh fhfhf hf hf fh fhf hf hf fh fhf hf hf f f fhf hf h', 0),
(2, 345, 'kboy boyk', '', 'fjfjfj fhfhfhfh fhfhf hf hf fh fhf hf hf fh fhf hf hf f f fhf hf h', 0),
(3, 47, 'kingsonly13', '', '                      \n                    fffffffff', 0),
(4, 47, 'kingsonly13', '', '                    ddddd', 0),
(5, 47, 'kingsonly13', '', '                    rrrrr', 0);
