	<section id="about-us">
        <div class="container">			
			<div class="skill-wrap clearfix">			
				<div class="center wow fadeInDown">
					<h2>About <span>Us</span></h2>
					<p class="lead">The company was founded by a team of enthusiastic humanitarian specialists who wanted to overcome the routine <br>and create a platform that would act in the market not only for business success but for the sake of <br>humanitarian and financial empowerment services.</p>
					<h3>OUR MISSION</h3>
					<p >This platform was created with the following mission: "to promote the greatest good, with a particular emphasis on helping man and the environment.</p>
				</div>
            </div>
           <div class="center wow fadeInDown">
                <h2>Core <span>Values</span></h2>
            </div>

            <div class="row">
                <div class="features">
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-laptop"></i>
                            <h2>IT SECURITY</h2>
                            <h3>An automated  security systems in place to ensure data integrity.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-comments"></i>
                            <h2>RESPONSIVE TEAM</h2>
                            <h3>Our team of experts monitor transactions to ensure all downlines are properly filled</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-cloud-download"></i>
                            <h2>IMMEDIATE PAY</h2>
                            <h3>In 3-5 working days, your interest is paid directly into your account!</h3>
                        </div>
                    </div><!--/.col-md-4-->

                </div><!--/.services-->
            </div><!--/.row-->    
			
			<!-- our-team -->
			
		</div><!--/.container-->
    </section><!--/about-us-->