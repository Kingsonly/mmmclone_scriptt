<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="http://almsaeedstudio.com">ADBN</a>.</strong> All rights
    reserved.
  </footer>
  
  </div>