 

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <body ng-app="mlmsoft" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>J</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Jackobiann</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
            
           
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              
              <span class="hidden-xs"><?php echo $user_details[0]['firstname'].' '.$user_details[0]['lastname'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header" style="height:auto;padding-bottom:0;">
                

                <p>
                  <?php echo $user_details[0]['firstname'].' '.$user_details[0]['lastname'];?> - 
                  <small>Member</small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo URL; ?>dashboard/logout/" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
          
    <p style="font-weight:;color:#fff" class="text-center"><?php echo $user_details[0]['firstname'].' '.$user_details[0]['lastname'];?><br/>
          <small>Member</small>
          </p>
        
      </div>

      
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="<?php echo URL.'dashboard';?>">
            <i class="fa fa-dashboard"></i> <span>Overview</span>
            
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo URL.'dashboard/profile';?>">
            <i class="fa fa-group"></i>
            <span>Profile</span>
            
          </a>
          
        </li>
        <li class="treeview">
          <a href="<?php echo URL.'dashboard/complains';?>">
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
            <span>Make complain</span>
            
          </a>
          
        </li>
        <li>
          <a href="<?php echo URL.'dashboard/disableaccount';?>">
            <i class="fa fa-ban" aria-hidden="true"></i> <span>Disable Account</span>
            
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo URL.'dashboard/extention';?>">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <span>Request Extention</span>
            
          </a>
        </li>
          
        <li class="treeview">
          <a href="<?php echo URL.'dashboard/testimony';?>">
            <i class="fa fa-quote-left" aria-hidden="true"></i>
            <span>Testimony</span>
            
          </a>
        </li>
        
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  

  