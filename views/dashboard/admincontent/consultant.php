<div class="content-wrapper" ng-controller="consultantController">
<section class="content">

<h3>Consultants List</h3>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">List Of Consultants</h3>
              <a href="<?php echo URL;?>admindashboard/registerconsultant" class="btn btn-default">+ Register Consultant</a>
              <div class="box-tools">
              
              <div class="input-group input-group-sm" style="width: 350px;float:right">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" ng-model="consultantSearch">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                   
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table  table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Link</th>
                  <th>Phone Number</th>
                  <th style="width: 40px">Subordinates</th>
                  <th style="width: 40px">Status</th>
                  <th style="width: 40px">&nbsp;</th>
                </tr>
                <tr ng-repeat="consultant in consultant | filter: consultantSearch">
                  <td>{{$index + 1}}</td>
                  <td><strong><a href="http://{{dirlocation}}/mlmsoft/admindashboard/generation?getdetails={{consultant.id}}">{{consultant.f_name}} {{consultant.l_name}}</a></strong></td>
                  <td>
                   http://www.yourdomain.com/{{consultant.ref_code}}
                  </td>
                  <td>{{consultant.phones}}</td>
                  <td><span class="badge bg-red">55%</span></td>
                  <td><span class="badge bg-red" ng-if="consultant.status=='1'">Active</span>
                  <span class="badge bg-green" ng-if="consultant.status=='0'">Inactive</span></td>
                  <td><a href="http://{{dirlocation}}/mlmsoft/admindashboard/registerconsultant?getdetails={{consultant.id}}"><span class="badge bg-default">Edit</span></a></td>
                </tr>
                
                
                
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
    </section>
</div>