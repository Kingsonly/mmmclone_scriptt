<div class="content-wrapper" ng-controller="generationController">
<section class="content">

      <div class="row">
        <div class="col-md-8">
            <div class="box box-success">
			 <div class="box-body">

<h4>GENERATION THREAD  <a href="<?php echo URL;?>admindashboard/registerconsultant?refcode={{consultant.ref_code}}" class="btn btn-default pull-right" style="margin-bottom:5px">+ Register Consultant</a></h4>
<div style="clear:both"></div>

      <div class="tree">
    <ul>
        <li>
            <span class="btn btn-info" style="border:none"><i class="icon-calendar"></i> <i class="fa fa-user bg-aqua"></i> {{consultant.f_name | uppercase}} {{consultant.l_name | uppercase}}'S PARENT(s)</span>
            <ul>
            
            <li>
            <span ng-repeat="fathers in fathers">
            
	          <a href="http://{{dirlocation}}/mlmsoft/admindashboard/generation?getdetails={{fathers.id}}"  style="margin-right:10px">  <i class="glyphicon glyphicon-user"></i> {{fathers.f_name}} {{fathers.l_name}}</a>
              </span>
             </li>
            </ul>
            
            <span class="btn btn-info" style="border:none;margin-top:10px"><i class="icon-calendar"></i> <i class="fa fa-user bg-aqua"></i> {{consultant.f_name | uppercase}} {{consultant.l_name | uppercase}}'S GENERATION</span>
            
            <ul>
                <li>
                	<span class="badge badge-primary"><i class="icon-minus-sign"></i> First Generation</span>
                    <ul>
                        <li>
	                       <span ng-repeat="sons in sons" ng-if="generator = getSplit(params,sons.gen)==0"><a href="http://{{dirlocation}}/mlmsoft/admindashboard/generation?getdetails={{sons.id}}"><i class="glyphicon glyphicon-user"></i> {{sons.f_name}} {{sons.l_name}}</a></span>
                        </li>
                    </ul>
                </li>
                <li>
                	<span class="badge badge-success"><i class="icon-minus-sign"></i> Second Generation</span>
                    <ul>
                        <li>
	                       <span ng-repeat="sons in sons" ng-if="generator = getSplit(params,sons.gen)==1"> <a href="http://{{dirlocation}}/mlmsoft/admindashboard/generation?getdetails={{sons.id}}"> <i class="glyphicon glyphicon-user"></i> {{sons.f_name}} {{sons.l_name}}</a></span>
                        </li>
                        
                    </ul>
                </li>
                <li>
                	<span class="badge badge-warning"><i class="icon-minus-sign"></i> Third Generation</span>
                    <ul>
                        <li>
	                       <span ng-repeat="sons in sons" ng-if="generator = getSplit(params,sons.gen)==2"><a href="http://{{dirlocation}}/mlmsoft/admindashboard/generation?getdetails={{sons.id}}"><i class="glyphicon glyphicon-user"></i> {{sons.f_name}} {{sons.l_name}}</a></span>
                        </li>
                        
                    </ul>
                </li>
                <li>
                	<span class="badge badge-important"><i class="icon-minus-sign"></i> Fourth Generation</span>
                    <ul>
                        <li>
	                       <span ng-repeat="sons in sons" ng-if="generator = getSplit(params,sons.gen)==3"><a href="http://{{dirlocation}}/mlmsoft/admindashboard/generation?getdetails={{sons.id}}"><i class="glyphicon glyphicon-user"></i> {{sons.f_name}} {{sons.l_name}}</a></span>
                        </li>
                    </ul>
                </li>
                
                <li>
                	<span class="badge badge-important"><i class="icon-minus-sign"></i> Fifth Generation</span>
                    <ul>
                        <li>
	                       <span ng-repeat="sons in sons" ng-if="generator = getSplit(params,sons.gen)==4"><a href="http://{{dirlocation}}/mlmsoft/admindashboard/generation?getdetails={{sons.id}}"><i class="glyphicon glyphicon-user"></i> {{sons.f_name}} {{sons.l_name}}</a></span>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </li>
        
    </ul>
</div>
      <!-- /.row -->
      </div>
      </div>
      </div>
      </div>
    </section>
</div>

<style>
.tree {
    min-height:20px;
    padding:19px;
    margin-bottom:20px;
    background-color:#fbfbfb;
    border:1px solid #999;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)
}
.tree li {
    list-style-type:none;
    margin:0;
    padding:10px 5px 0 5px;
    position:relative
}
.tree li::before, .tree li::after {
    content:'';
    left:-20px;
    position:absolute;
    right:auto
}
.tree li::before {
    border-left:1px solid #999;
    bottom:50px;
    height:100%;
    top:0;
    width:1px
}
.tree li::after {
    border-top:1px solid #999;
    height:20px;
    top:25px;
    width:25px
}
.tree li span {
    -moz-border-radius:5px;
    -webkit-border-radius:5px;
    border:1px solid #999;
    border-radius:5px;
    display:inline-block;
    padding:3px 8px;
    text-decoration:none;
	margin:4px;
	margin-left:0;
}
.tree li.parent_li>span {
    cursor:pointer
}
.tree>ul>li::before, .tree>ul>li::after {
    border:0
}
.tree li:last-child::before {
    height:30px
}
.tree li.parent_li>span:hover, .tree li.parent_li>span:hover+ul li span {
    background:#eee;
    border:1px solid #94a0b4;
    color:#000
}
</style>

<script>
$(function () {
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    $('.tree li.parent_li > span').on('click', function (e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
        } else {
            children.show('fast');
            $(this).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
        }
        e.stopPropagation();
    });
});
</script>