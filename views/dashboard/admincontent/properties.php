<div class="content-wrapper" ng-controller="propertyController">
<?php if(!isset($_GET['add'])){?>
<section class="content">
<h3>Properties And Details</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">List Of Properties <a href="?add" class="btn btn-info">Add a property</a></h3>
              <div class="box-tools">
              
              <div class="input-group input-group-sm" style="width: 350px;float:right">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" ng-model="propertiestSearch">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                   
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table  table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th width="60px">Photo</th>
                  <th width="60px">Name</th>
                  <th>Owner</th>
                  <th>ID</th>
                  <th>Price</th>
                  <th>Description</th>
                  <th>Document</th>
                  <th style="width: 40px">Location</th>
                  <th style="width: 40px">State</th>
                  <th style="width: 40px">Status</th>
                  <th></th>
                </tr>
                <tr ng-repeat="properties in properties | filter: propertiestSearch">
                  <td>{{$index + 1}}</td>
                  <td><img src="http://{{dirlocation}}/mlmsoft/{{properties.photo}}" class="img-responsive" style="width:100%" /></td>
                  <td><strong><a href="http://{{dirlocation}}/mlmsoft/admindashboard/viewproperty?get={{properties.property_id}}">{{properties.property_name}}</a></strong></td>
                  <td>{{properties.f_name}} {{properties.l_name}}</td>
                  <td>{{properties.ref_code}}</td>
                  <td>N{{properties.price}}</td>
                  <td>{{properties.description}}</td>
                  <td>{{properties.document}}</td>
                  <td>{{properties.location}}</td>
                  <td>{{properties.state}}</td>
                  <td><span class="badge bg-red" ng-if="properties.status=='1'">Active</span>
                  <span class="badge bg-green" ng-if="properties.status=='0'">Inactive</span></td>
                  <td><a class="badge bg-orange" href="#"><i class="fa fa-envelope"></i> Send</a></td>
                </tr>
                
                
                
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
    </section>
<?php }else{?>
<style>
.input-group, .form-control{margin-bottom:10px}
</style>

<section class="content">
<h3>Add a property</h3>
<div class="box box-info">
<div class="register-box-body">

    	<div id="result" class="alert alert-info col-lg-5" style="float:none;display:none; margin:auto;text-align:center;margin-bottom:10px"></div>
        
    <div class="loader" style="text-align:center;margin-bottom:10px;display:none">
     <img src="<?php echo URL;?>views/images/load1.gif" style="margin:auto;width:40px" />
    </div>
    
    <form id="saveproperty" ng-submit="saveproperty()">
   
    <div class="col-lg-6">
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> Property Name</button>
      </div> 
      <input type="text" name="property_name" class="form-control pull-right" placeholder="Property Name" required="required">
       
    </div>
      
     <textarea class="form-control pull-right" placeholder="Property Description" required="required" style="width:100%" name="description"></textarea>
  
  
     <textarea class="form-control pull-right" placeholder="Property Document" required="required" style="width:100%" name="document"></textarea>
    
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-upload"></i> Upload Image</button>
      </div> 
      <input type="file" name="photo" class="form-control pull-right" placeholder="Browse Property Image" required="required">
    </div>
      
   </div>
      
      
      <div class="col-lg-6">
    
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-group"></i> Consultant</button>
      </div> 
                <select class="form-control select2" style="width: 100%;" name="consultant_id">
                <option value="0">---Select Consultant --- </option>
                  <option ng-repeat="consultant in consultant" value="{{consultant.id}} ">{{consultant.f_name}}   {{consultant.l_name}}</option>
                </select>
       
    </div>
    
    
          <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-map-marker"></i> Location</button>
      </div> 
      <input type="text" name="location" class="form-control pull-right" placeholder="Location" required="required">
       
    </div>
    
    
    
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> State</button>
      </div> 
      <input type="text" name="state" class="form-control pull-right" placeholder="State" required="required">
    </div>
    
    
         
      
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-money"></i> Price</button>
      </div> 
      <input type="number" name="price" class="form-control pull-right" placeholder="Amount" required="required">
    </div>
    
     
    
    <input type="hidden" name="status" value="1">
     <input type="hidden" name="date" value="<?php echo date('Y-m-d');?>">
    <div class="col-xs-4" style="padding-left:0">
     <button type="submit" class="btn btn-primary btn-sm">SUBMIT</button>   
     </div>      
      </div>
      
      
      <div style="clear:both"></div>
    </form>
      <div style="clear:both"></div>
  </div>
  </div>
</section>
<?php }?>
</div>
