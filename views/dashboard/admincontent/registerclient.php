<style>
.input-group, .form-control{margin-bottom:10px}
</style>
<div class="content-wrapper" ng-controller="registerClientController">
<section class="content">
<h3>Register a Client</h3>
<div class="box box-info">
<div class="register-box-body">

    	<div id="result" class="alert alert-info col-lg-5" style="float:none;display:none; margin:auto;text-align:center;margin-bottom:10px"></div>
        
    <div class="loader" style="text-align:center;margin-bottom:10px;display:none">
     <img src="<?php echo URL;?>views/images/load1.gif" style="margin:auto;width:40px" />
    </div>
    
    <form id="saveclient" ng-submit="saveclient()">
   
    <div class="col-lg-6">
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-user"></i> First Name</button>
      </div> 
      <input type="text" name="fname" class="form-control pull-right" placeholder="First Name" required="required">
       
    </div>
    
     <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-user"></i> Last Name</button>
      </div> 
      <input type="text" name="lname" class="form-control pull-right" placeholder="Last Name" required="required">
       
    </div>
          
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-envelope"></i> Email</button>
      </div> 
      <input type="text" name="email" class="form-control pull-right" placeholder="Email Address" required="required">
       
    </div>  
    
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-phone"></i> Mobile</button>
      </div> 
      <input type="text" name="phone" class="form-control pull-right" placeholder="Phone Number" required="required">
       
    </div>

      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-group"></i> Consultant</button>
      </div> 
                <select class="form-control select2" style="width: 100%;" name="consultant_id">
                 
                  <option ng-repeat="consultant in consultant" value="{{consultant.id}}">{{consultant.f_name}} {{consultant.l_name}} ( {{consultant.ref_code}})</option>
                  
                </select>
       
    </div>
    
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> Property</button>
      </div> 
                <select class="form-control select2" style="width: 100%;" name="property_id">
                <option value="0">---Select Property --- </option>
                  <option ng-repeat="properties in properties" value="{{properties.id}} ">{{properties.property_name}}   N{{properties.price}}</option>
                </select>
       
    </div>
    
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-money"></i> Payment Method</button>
      </div> 
                <select class="form-control select2" style="width: 100%;" name="payment_method">
                  <option selected="selected" value="1">Cash</option>
                  <option value="2">Transfer</option>
                  
                </select>
       
    </div>
      
   </div>
      
      
      <div class="col-lg-6">

      <label>
      Gender: 
      </label>  
      
      <input type="radio" class="flat-red" name="gender" value="1" checked="checked"> Male <input type="radio" name="gender" class="flat-red" value="2"> Female
         
      	<textarea class="form-control pull-right" placeholder="Address" required="required" style="width:100%" name="address"></textarea>
    	 
        
        
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> City</button>
      </div> 
      <input type="text" name="city" class="form-control pull-right" placeholder="City" required="required">
    </div>  
    
    
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> State</button>
      </div> 
      <input type="text" name="state" class="form-control pull-right" placeholder="State" required="required">
    </div>
    
    
         <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> Country</button>
      </div> 
      <input type="text" name="country" class="form-control pull-right" placeholder="Country" required="required">
    </div>
      
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-money"></i> Amount Paid</button>
      </div> 
      <input type="number" name="amount_paid" class="form-control pull-right" placeholder="Amount" required="required">
    </div>
    
     
    
    <input type="hidden" name="status" value="1">
     <input type="hidden" name="date" value="<?php echo date('Y-m-d');?>">
    <div class="col-xs-4" style="padding-left:0">
     <button type="submit" class="btn btn-primary btn-sm">SUBMIT</button>   
     </div>      
      </div>
      
      
      <div style="clear:both"></div>
    </form>
      <div style="clear:both"></div>
  </div>
  </div>
</section>
</div>