<div class="content-wrapper" ng-controller="transactionsController">
<section class="content">
<h3>List of Transactions</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Recent Payment(s)</h3>
              <div class="box-tools">
              
              <div class="input-group input-group-sm" style="width: 350px;float:right">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" ng-model="transactionSearch">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                   
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table  table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th width="10px">Consultant Name</th>
                  <th>Mobile No</th>
                  <th width="130px">Client's Name</th>
                  <th>Property</th>
                  <th>Ref. ID</th>
                  <th>Total Cost</th>
                  <th>Amount Paid</th>
                  <th>Balance</th>
                  <th>Date of payment</th>
                  
                </tr>
                <tr ng-repeat="transactions in transactions | filter: transactionsSearch">
                  <td>{{$index + 1}}</td>
                  <td width="150px">{{transactions.f_name}} {{transactions.l_name}}</td>
                  <td>{{transactions.phones}}</td>
                  <td>{{transactions.fname}} {{transactions.lname}}</td>
                  <td>{{transactions.property_name}}</td>
                  <td>{{transactions.payment_rand}}</td>
                  <td>N{{transactions.price}}</td>
                  <td>N{{transactions.amount_paid}}</td>
                  <td><strong>N{{transactions.price - transactions.amount_paid}}</strong></td>
                  <td>{{transactions.payment_date}}</td>
                  
                </tr>
                
                
                
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
    </section>
</div>