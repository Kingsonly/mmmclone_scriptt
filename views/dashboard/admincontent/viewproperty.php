<style>
.input-group, .form-control{margin-bottom:10px}
</style>

<div class="content-wrapper" ng-controller="viewpropertyController">
<section class="content">
<div class="col-lg-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#description" data-toggle="tab">Description</a></li>
              <li><a href="#edit" data-toggle="tab">Edit</a></li>
              
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="description">
                <!-- Post -->
                <img src="http://{{dirlocation}}/mlmsoft/{{property.photo}}" style="width:90%;float:none;margin:auto" class="img-responsive thumbnail">
                <div class="post">
                  <div class="user-block">
                        <span class="username">
                          <h3><a href="#">{{property.property_name}}</a></h3>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                    <span class="description">Date published - {{property.date}}<span class="pull-right">Location: {{property.location}}/{{property.state}} </span></span>
                    
                   <span class="description">
                   <strong> Description: </strong>{{property.description}}
                  </span>
                  
                  <span class="description">
                   <strong> Document: </strong>{{property.document}}
                  </span>
                  
                   <span class="description">
                   <span class="badge bg-red pull-right">
                   <h4><strong> Price: N</strong>{{property.price}}</h4>
                   </span>
                  </span>
                  
                  
                  </div>
                  
                  
                  <!-- /.user-block -->
                 
                  
                </div>
              </div>
              <!-- /.tab-pane -->
              
              
              <div class="tab-pane" id="edit">
                <!-- The timeline -->
                
                   	<div id="result" class="alert alert-info col-lg-5" style="float:none;display:none; margin:auto;text-align:center;margin-bottom:10px"></div>
        
    <div class="loader" style="text-align:center;margin-bottom:10px;display:none">
     <img src="<?php echo URL;?>views/images/load1.gif" style="margin:auto;width:40px" />
    </div>


                <form id="editproperty" ng-submit="editproperty()">
   
    <div class="col-lg-6">
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> Property Name</button>
      </div> 
      <input type="text" name="property_name" class="form-control pull-right" value="{{property.property_name}}" placeholder="Property Name" required>
       
    </div>
      
     <textarea class="form-control pull-right" placeholder="Property Description" required style="width:100%" name="description">{{property.description}}</textarea>
  
  
     <textarea class="form-control pull-right" placeholder="Property Document" required style="width:100%" name="document">{{property.document}}</textarea>
    
     
     <button class="btn btn-default btn-sm" type="button" ng-click="showdiv=true">Change property image x</button>

    <div class="input-group has-feedback" style="" ng-show="showdiv">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-upload"></i> Upload Image</button>
      </div> 
      <input type="file" name="photo" class="form-control pull-right" placeholder="Browse Property Image" required>
    </div>
      
   </div>
      
      
      <div class="col-lg-6">
    
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-group"></i> Consultant</button>
      </div> 
                <select class="form-control select2" style="width: 100%;" name="consultant_id">
                <option value="0">---Select Consultant --- </option>
                <option ng-repeat="consultant in consultant" value="{{consultant.id}}" ng-selected="property.consultant_id == consultant.id">{{consultant.f_name}}   {{consultant.l_name}}</option>

                </select>
       
    </div>
    
    
          <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-map-marker"></i> Location</button>
      </div> 
      <input type="text" name="location" class="form-control pull-right" placeholder="Location" required value="{{property.location}}">
       
    </div>
    
    
    
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> State</button>
      </div> 
      <input type="text" name="state" class="form-control pull-right" placeholder="State" required value="{{property.state}}">
    </div>
    
    
         
      
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-money"></i> Price</button>
      </div> 
      <input type="number" name="price" class="form-control pull-right" placeholder="Amount" required value="{{property.price}}">
    </div>
    
     
    
    <input type="hidden" name="status" value="1">
     <input type="hidden" name="date" value="<?php echo date('Y-m-d');?>">
    <div class="col-xs-4" style="padding-left:0">
     <button type="submit" class="btn btn-primary btn-sm">SUBMIT</button>   
     </div>      
      </div>
      
      
      <div style="clear:both"></div>
    </form>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          </div>
          <div style="clear:both"></div>
          </section>
          <!-- /.nav-tabs-custom -->
        </div>