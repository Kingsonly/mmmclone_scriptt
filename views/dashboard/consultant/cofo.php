<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    

       <section class="content">
           
           <p> Land Documents explained </p>
                    <br>
                    <p><b>SURVEY PLAN</b></p>
                    <p>A Survey plan is a document that measures the boundary of a parcel of land to give an accurate measurement and description of that land. The people that handle survey issues are Surveyors and they are regulated by the office of the Surveyor general in Lagos as it relates to survey issues in Lagos. A survey plan must contain the following information:</p>
                    <p>1. The name of the owner of the land surveyed</p>
                    <p>2. The Address or description of the land surveyed</p>
                    <p>3. The size of the land surveyed</p>
                    <p>4. The drawn out portion of the land survey and mapped out on the survey plan document</p>
                    <p>5. The beacon numbers</p>
                    <p>6. The surveyor who drew up the survey plan and the date it was drawn up</p>
                    <p>7. A stamp showing the land is either free from Government acquisition or not</p>
                    <br>
                    <p><b>EXCISION</b></p>
                    <p>Land Use Decree on the 28th of March, 1978 that vested all lands in every state of the Federation under the control of the State Governors. The Land Use Act coupled with other laws made it possible for the Governor who was now the owner of all lands in the state to actually have the power to Acquire more lands compulsorily for its own public purpose to provide amenities for the greater good of the citizens.</p>
                    <p>Fortunately, the government recognizes that indigenes of different sections of the country have a right to existence . . . a right to the land of their birth. Hence, it is customary for state government to cede a portion of land to the original owners (natives) of each area.</p>
                    <p>An Excision means basically taking a part from a whole and that part that has been excised, will be recorded and documented in the official government gazette of that state.</p>
                    <br>
                    <p><b>GAZETTE</b></p>
                    <p>A Gazette is an Official record book where all special government details are spelt out, detailed and recorded</p>
                    <p>A gazette will show the communities or villages that have been granted excision and the number of acres or hectares of land that the government has given to them. It is within those excised acres or hectares that the traditional family is entitled to sell its lands to the public and not anything outside those hectares of land given or excised to them.</p>
                    <p>A Gazette is a very powerful instrument the community owns and can replace a Certificate of Occupancy to grant title to the Villagers. A community owning a gazette can only sell lands to an individual within those lands that have been excised to them and the community or family head of that land has the right to sign your documents for you if you purchase lands within those excised acres or hectares of land.</p>
                    <br>
                    <p><b>DEED OF ASSIGNMENT</b></p>
                    <p>A Deed of Assignment is an Agreement between the Seller of a Land or Property and a Buyer of that Land or property showing evidence that the Seller has transferred all his rights, his title, his interest and ownership of that land to that the Seller that has just bought land.</p>
                    <p>The Deed of Assignment has been exchanged between both parties, it has to be recorded in the land registry to show legal proof that the land has exchanged hands and the public should be aware of the transaction. Such recorded Deed of Assignment come in the form of either a Governorâ€™s Consent or _Registered Conveyance</p>
                    <br>
                    <p><b>CERTIFICATE OF OCCUPANCY</b></p>
                    <p>A Certificate of Occupancy (C of O) issued by the Lagos State Government officially leases Lagos land to you, the applicant, for 99 yrs. As already indicated above, all lands belong to the Government.</p>
                    <p>A C of O however is the officially recognized Document for demonstrating Right to a Land.<br>
                      What happens after 99 years? That question is still subject of debate among experts. Most have adopted a wait-and-see attitude. Others postulate that as the new owner of the land, you the buyer, can renew the certificate of occupancy when it expires. That makes sense, but for now is largely a case of â€œWe shall see when we get thereâ€<br>
                      <br>
                      <br>
                    </p>
                    <p>                       </p>

     

     
      <!-- /.example-modal -->

     

     
      

     

     
    </section>
      </div>



	
