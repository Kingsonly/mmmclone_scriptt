<!-- Content Wrapper. Contains page content -->
<style>
    .tables{
        background: #3C8DBC;
        color: #fff;
    }
</style>





  <div class="content-wrapper">
      
      
      
      
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section> 

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-12 col-sm-6" style="height:200px;">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner" style="height:150px;">
              <h3>
                  
                  <?php 
                  if($user_details[0]['plan']==50){
                      echo "Start Up";
                      
                  }
                  if($user_details[0]['plan']==100){
                      echo "Standard";
                      
                  }
                  if($user_details[0]['plan']==200){
                      echo "Premium";
                      
                  }
                  
                  ?>
                
                </h3> 
                <h3 style="font-size:20px;">
                    ₦<?php echo $user_details[0]['plan'] ?>,000
                </h3>
                
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer"><h3 style="font-size:15px;">Current Package</h3> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-12 col-sm-6"  style="height:200px;">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner" style="height:150px;">
              <h3>Transactions</sup></h3>
              <h3>
                  <p>Total GH :<?php echo  !empty($gh_count)?$gh_count:0; ?></p>
                  <p>Total PH :<?php echo  !empty($ph_count)?$ph_count:0; ?></p>
              </h3>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer"><h3 style="font-size:15px;">Status </h3> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-12 col-sm-6"  style="height:200px;">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner" style="height:150px;">
              <h3>
                <?php 
                    if($user_details[0]['status']==2 or $user_details[0]['status'] ==5 or $user_details[0]['status'] ==6 ){
                        echo 'MERGED';
                    }else{ echo '<p>NOT YET MERGED</p>';}
                  ?>
                
                </h3>

              <h3><?php if($user_details[0]['status']==2 or $user_details[0]['status']==5 or $user_details[0]['status']==6 ){echo '₦'.$user_details[0]['plan'].',000' ;}
                  
                  ?>
                  </h3>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer"><h3 style="font-size:15px"><?php if($user_details[0]['status']==2 or $user_details[0]['status']==5 or $user_details[0]['status']==8 ){echo "Merged to";}else{echo "Pending";}?><?php if($user_details[0]['status'] ==5){echo 'PAY';}elseif($user_details[0]['status'] ==2){echo 'RECIVE';} ?> </h3> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-12 col-sm-6"  style="height:200px;">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner" style="height:150px;">
              <h3 ></h3>

              <h3 class="demo1" style="font-size:18px"></h3>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer"> <h3 style="font-size:15px">Time left</h3> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      
      
        
      
       
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php if(!empty($get_ph1)){echo "Provide Help";}elseif(!empty($get_gh1)){echo "Get Help";}?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            
                   <div class="box-body">
               
             
                <?php  if(!empty($get_ph)){?>
                   <?php foreach($get_ph as $k=>$v){ ?>
                       
                      <div class="callout callout-info">
                          <table class="table">
                              <tr>
                                  <th>Account Name</th>
                                  <th>Phone Number</th>
                                  <th>Bank Name</th>
                                  <th>Account Number</th>
                                  <th>Amout To Pay</th>
                                  <th></th>
                              </tr>
                          <tr>
                            <td>
                                <h4><?php echo $v['account_name'] ?></h4>
                              </td> 
                            <td>
                                <h4><?php echo $v['phone']; ?></h4>
                              </td>
                            <td>
                                <h4>
                                    <?php //var_dump(date("M-d-Y H:m:s", strtotime('+5 hours'))); ?><?php echo $v['bank_name']; ?>
                                </h4>
                              </td> 
                               <td>
                                   <h4><?php echo $v['account_number']; ?></h4>
                              </td> 
                               <td>
                                   <h4><?php echo $v['plan']; ?></h4>
                              </td> 
                              
                          </tr>
                          <tr>
                              <td>
                              <h4><button class="btn bg-orange margin phb" type="button" data-mainid="<?php echo $v['id'] ?>"  data-status="<?php echo $user_details[0]['status']?>" data-id="<?php echo $v["ph_id"]?>">Payment Made</button>
                              </td>
                              <td><h4>Time Left:</h4></td>
                            <td colspan="3"> <h4 class="demo" style="color:red;"></h4>
                                

                            <script>
                                $('.demo1').countdown('<?php echo $v['end_time'] ?>', function(event) {
                                    $(this).html(event.strftime('%w weeks %d days %H:%M:%S'));
                                  });
                            /*/ Set the date we're counting down to
                            var countDownDate = new Date("<?php echo $v['end_time'] ?>").getTime();

                            // Update the count down every 1 second
                            var x = setInterval(function() {

                              // Get todays date and time
                              var now = new Date().getTime();

                              // Find the distance between now an the count down date
                              var distance = countDownDate - now;

                              // Time calculations for days, hours, minutes and seconds
                              var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                              var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                              var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                
                              // Display the result in the element with id="demo"
                              document.getElementById("demo").innerHTML = days + "d " + hours + "h "
                              + minutes + "m " + seconds + "s ";

                              // If the count down is finished, write some text
                              if (distance < 0) {
                                clearInterval(x);
                                document.getElementById("demo").innerHTML = "EXPIRED";
                              }
                            }, 1000);*/
                            </script>
                            
                              </td> 
                          </tr>
                          
                          </table>
                         
                          
                        
                         
                          

                       
                      </div>
                    
                   <?php } ?>
                <?php }elseif(!empty($get_gh)){ ?>
             
             <?php foreach($get_gh as $k=>$v){ ?>
                      <div class="callout callout-success">
                       <table class="table">
                              <tr>
                                  <th>PH Name</th>
                                  <th>Phone Number</th>
                                  <th>Amout To Recive</th>
                                  <th>Payment Status</th>
                                  <th>Approval</th>
                              </tr>
                          <tr>
                            <td>
                                <h4><?php echo $v['firstname'].' '.$v['lastname'] ?></h4>
                              </td> 
                            <td>
                                <h4><?php echo $v['phone']; ?></h4>
                              </td>
                            <td>
                                <h4>
                                    <?php //var_dump(date("M-d-Y H:m:s", strtotime('+5 hours'))); ?><?php echo $v['plan']; ?>
                                </h4>
                              </td> 
                               <td>
                                   <h4><?php echo $v['ph_status']==0?'Pending':'Paid' ?></h4>
                              </td> 
                               <td>
                                   <h4><button class="btn bg-orange margin ghb" type="button" data-mainid="<?php echo $v['id'] ?>"  data-phid="<?php echo $v["ph_id"]?>" data-id="<?php echo $v["gh_id"]?>">Confirm</button></h4>
                              </td> 
                              
                          </tr>
                          <tr>
                              <td>
                              
                              </td>
                              <td><h4>Time Left:</h4></td>
                            <td colspan="4"> <h4 id="demo" style="color:red;"></h4>

                            <script>
                                 $('.demo1').countdown('<?php echo $v['end_time'] ?>', function(event) {
                                    $(this).html(event.strftime('%w weeks %d days %H:%M:%S'));
                                  });/*
                            // Set the date we're counting down to
                            var countDownDate = new Date("<?php echo $v['end_time'] ?>").getTime();

                            // Update the count down every 1 second
                            var x = setInterval(function() {

                              // Get todays date and time
                              var now = new Date().getTime();

                              // Find the distance between now an the count down date
                              var distance = countDownDate - now;

                              // Time calculations for days, hours, minutes and seconds
                              var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                              var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                              var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                
                              // Display the result in the element with id="demo"
                              document.getElementById("demo").innerHTML = days + "d " + hours + "h "
                              + minutes + "m " + seconds + "s ";

                              // If the count down is finished, write some text
                              if (distance < 0) {
                                clearInterval(x);
                                document.getElementById("demo").innerHTML = "EXPIRED";
                              }
                            }, 1000);*/
                            </script>
                            
                              </td> 
                          </tr>
                          
                          </table>
                      </div>
                    
                   <?php } ?>
                  
            <?php }else{ ?>
                
                <div class="callout callout-danger">
                    Sorry for the Delay You  would be merged shortly
                      
                      </div>
                
            <?php } ?>
                       </div>
            </div>
            
            <!-- /.box-body -->
      
      
      
      
         



        
        <!-- ./col -->
     

      
    
</section>
      
      
      <div class="row">
        <div class="col-md-4">
          <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">How to confirm Payment</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipiscing elit</li>
                <li>Integer molestie lorem at massa</li>
                <li>Facilisis in pretium nisl aliquet</li>
                <li>Nulla volutpat aliquam velit
                  <ul>
                    <li>Phasellus iaculis neque</li>
                    <li>Purus sodales ultricies</li>
                    <li>Vestibulum laoreet porttitor sem</li>
                    <li>Ac tristique libero volutpat at</li>
                  </ul>
                </li>
                <li>Faucibus porta lacus fringilla vel</li>
                <li>Aenean sit amet erat nunc</li>
                <li>Eget porttitor lorem</li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <div class="col-md-4">
          <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">How to indicate payment made</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ol>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipiscing elit</li>
                <li>Integer molestie lorem at massa</li>
                <li>Facilisis in pretium nisl aliquet</li>
                <li>Nulla volutpat aliquam velit
                  <ol>
                    <li>Phasellus iaculis neque</li>
                    <li>Purus sodales ultricies</li>
                    <li>Vestibulum laoreet porttitor sem</li>
                    <li>Ac tristique libero volutpat at</li>
                  </ol>
                </li>
                <li>Faucibus porta lacus fringilla vel</li>
                <li>Aenean sit amet erat nunc</li>
                <li>Eget porttitor lorem</li>
              </ol>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <div class="col-md-4">
          <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">How to make a complain</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="list-unstyled">
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipiscing elit</li>
                <li>Integer molestie lorem at massa</li>
                <li>Facilisis in pretium nisl aliquet</li>
                <li>Nulla volutpat aliquam velit
                  <ul>
                    <li>Phasellus iaculis neque</li>
                    <li>Purus sodales ultricies</li>
                    <li>Vestibulum laoreet porttitor sem</li>
                    <li>Ac tristique libero volutpat at</li>
                  </ul>
                </li>
                <li>Faucibus porta lacus fringilla vel</li>
                <li>Aenean sit amet erat nunc</li>
                <li>Eget porttitor lorem</li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
      </div>
      
      
      
      
      
     <div class="row">
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Testimony</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if(!empty($testimonials)){ foreach($testimonials as $k=>$v){ ?>
                
                <div class="box-body">
              <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php if(file_exists(URL.$v['mem_pix'])){echo URL.$v['mem_pix'];}else{if($user_details[0]['gender']==1){echo URL.'public/dist/img/avatar.png';}else{ echo URL.'public/dist/img/avatar2.png';}}?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $v['mem_name']?></h3>
                <h3 class="profile-username text-center"><?php echo $v['mem_comment']?></h3>
                  <hr>

              
            </div>
            </div>
                <?php }} ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Profile</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php if(file_exists($user_details[0]['pic_path'])){echo URL.$user_details[0]['pic_path'];}else{if($user_details[0]['gender']==1){echo URL.'public/dist/img/avatar.png';}else{ echo URL.'public/dist/img/avatar2.png';}}?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $user_details[0]['firstname'].' '.$user_details[0]['lastname'];?></h3>

              

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Gender</b> <a class="pull-right"><?php echo $user_details[0]['gender']==1?'Male':'Female'; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Date of birth</b> <a class="pull-right"><?php echo $user_details[0]['dob']; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Total Earning</b> <a class="pull-right"><?php echo $gh_count * $user_details[0]['plan'];?>,000</a>
                </li>
                  
                  <li class="list-group-item">
                  <b>Total GH</b> <a class="pull-right"><?php echo $gh_count;?></a> 
                </li>
                  
                  <li class="list-group-item">
                  <b>Total PH</b> <a class="pull-right"><?php echo $ph_count;?></a>
                </li>
                  
                  <li class="list-group-item">
                  <b>Account Status</b> <a class="pull-right"><?php if($user_details[0]['status']==0 or $user_details[0]['status']==5){echo 'Provide Help';}else{echo 'Get Help';}?></a>
                </li>
              </ul>

              
            </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
      </div>

             
      
      

       
      </div>

	
