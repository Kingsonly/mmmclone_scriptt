<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    

    

       <section class="content">
      <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Adloyalty, Inc.
            <small class="pull-right">Date: <?php echo date('Y-m-d') ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Adloyalty, Inc.</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@adloyalty.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php echo $user_details[0]['f_name'].' '.$user_details[0]['f_name']; ?></strong><br>
            
            Address:<?php echo $user_details[0]['address'];?><br>
            Phone: <?php echo $user_details[0]['phones'];?><br>
            Email: <?php echo $user_details[0]['email'];?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        <!--    <b>Invoice #007612</b><br>
          <br>
          <b>Order ID:</b> 4F3S8J<br>
          <b>Payment Due:</b> 2/22/2014<br>
          <b>Account:</b> 968-34567 /.col -->
        
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Amount</th>
              <th>Date</th>
              <th>Date due</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td><?php echo $invoice['amount']?></td>
              <td> <?php $date=date_create($invoice['dates']); echo date_format($date,"Y/m/d");?>  </td>
              <td><?php $date=strtotime("+5 day", strtotime($invoice['dates'])); echo date("Y/m/d", $date);?></td>
              <td><?php echo $invoice['status']==1? 'Pending':'Paid'; ?></td>
            </tr>
            
            
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
         <?php echo $invoice['type']==1? 'Bank transfer':'Cash'; ?>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount Due <?php $date=strtotime("+5 day", strtotime($invoice['dates'])); echo date("Y/m/d", $date);?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Withdrawal:</th>
                <td><?php echo $invoice['amount']?></td>
              </tr>
              <tr>
                <th>Account ballace</th>
                <td><?php echo $invoice['ballance']?></td>
              </tr>
              
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
     
    </section>

</section>
      </div>



	
