
<div id="about-slider" >
                <div id="carousel-slider" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators visible-xs">
                        <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-slider" data-slide-to="1"></li>
                        <li data-target="#carousel-slider" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="public/images/slider_one.jpg" class="img-responsive" alt=""> 
                       </div>
                       <div class="item">
                            <img src="public/images/slider_one.jpg" class="img-responsive" alt=""> 
                       </div> 
                       <div class="item">
                            <img src="public/images/slider_one.jpg" class="img-responsive" alt=""> 
                       </div> 
                    </div>
                    
                    <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
                        <i class="fa fa-angle-left"></i> 
                    </a>
                    
                    <a class=" right carousel-control hidden-xs" href="#carousel-slider" data-slide="next">
                        <i class="fa fa-angle-right"></i> 
                    </a>
                </div> <!--/#carousel-slider-->
            </div><!--/#about-slider-->


<section class="pricing-area shortcode-item">
        <div class="container">
        <div class="center wow fadeInDown">
            <h2>Our Packages</h2>
            <p>Select exclusive packages that fits you</p></div>
            <div class="row text-center">
                <div class="col-sm-4 plan price-one wow fadeInDown">
                    <ul>
                        <li class="heading-one">
                            <h3>Start Up</h3>
                            <span>N50,000/24hr-5 Days</span>
                        </li>
                        <li>2:1 Matrix</li>
                        <li>Auto Design</li>
                        <li>Cash Out</li>
                        <li>Auto Recycle</li>
                        <li>N100,000 Return Investment</li>
                        <li class="plan-action">
                            <a href="./registration?recval=<?php echo md5('50') ?>" class="btn btn-primary">Sign up</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-4 plan price-two wow fadeInDown">
                    <ul>
                        <li class="heading-two">
                            <h3>Standard</h3>
                            <span>N100,000/24-5 Days</span>
                        </li>
                        <li>2:1 Matrix</li>
                        <li>Auto Design</li>
                        <li>Cash Out</li>
                        <li>Auto Recycle</li>
                        <li>N200,000 Return Investment</li>
                        <li class="plan-action">
                            <a href="./registration?recval=<?php echo md5('100') ?>" class="btn btn-primary">Sign up</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-4 plan price-three wow fadeInDown">
                    <img src="public/images/ribon_one.png">
                    <ul>
                        <li class="heading-three">
                            <h3>Premium</h3>
                            <span>N200,000/24-5 Days</span>
                        </li>
                        <li>2:1 Matrix</li>
                        <li>Auto Design</li>
                        <li>Cash Out</li>
                        <li>Auto Recycle</li>
                        <li>N400,000 Return Investment</li>
                        <li class="plan-action">
                            <a href="#" class="btn btn-primary">Coming Soon</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!--/pricing_area-->

 <section id="portfolio">
        <div class="container">
            <div class="center">
               <h2>Testimonials</h2>
               <p class="lead">See how people from  different packages have benefitted from this awsome opportunity.</p>
            </div>
        

            <ul class="portfolio-filter text-center">
                <li><a class="btn btn-primary active" href="#" data-filter="*">ALL</a></li>
                <li><a class="btn btn-primary" href="#" data-filter=".bootstrap">BASIC</a></li>
                <li><a class="btn btn-primary" href="#" data-filter=".html">STANDARD</a></li>
                <li><a class="btn btn-primary" href="#" data-filter=".abc">PREMIUM</a></li>
            </ul><!--/#portfolio-filter-->

            <div class="row">
                <div class="portfolio-items">
                    

                   

                    

                    

                   <!--would use  <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item8.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                                                        <h3><a href="#">Mr L. T Akinde</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p>
                                    <a class="preview" href="public/images/portfolio/full/item8.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>          
                    </div><!--/.portfolio-item-->
                </div>
            </div>
        </div>
    </section><!--/#portfolio-item-->

        <section id="about-us">
        <div class="container">         
            <div class="skill-wrap clearfix">           
                <div class="center wow fadeInDown">
                    <h2>Members In<span> Our Packages</span></h2>
                </div>
                
                <div class="row">       
                    <div class="col-sm-4">
                        <div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <div class="joomla-skill">                                   
                                <p><em>855</em></p>
                                <p>BASIC</p>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-sm-4">
                        <div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="200ms">
                            <div class="html-skill">                                  
                                <p><em>200</em></p>
                                <p>STANDARD</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">
                        <div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
                            <div class="css-skill">                                    
                                <p><em>800</em></p>
                                <p>PREMIUM</p>
                            </div>
                        </div>
                    </div>                  
                </div>  
            </div>
            </div></section>

    <section id="conatcat-info">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="media contact-info wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                     
                        <div class="media-body">
                            <h2>Ready to get started?</h2>
                            <p>Simply create an account, make a donation and get 100% back.<br>You also get a registration bonus when you register on or before 30th April 2017.<br> <h4><a class="btn btn-primary" href="<?php echo URL ?>./registerplan">CREATE ACCOUNT</a></h4></p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/.container-->    
    </section><!--/#conatcat-info-->