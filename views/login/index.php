

  <!-- /.login-logo -->
  
  <!-- /.login-box-body -->





<section id="about-us">
        <div class="container">			
			<div class="skill-wrap clearfix">			
				<div class="center wow fadeInDown">
                
					<div class="login-box-body " style="margin:0 auto; width:300px;">
                <h2> <span>Sign in to start.</span></h2>

    <form action="./login/run" method="post">
    <?php if(!empty($wrong_password)){?>    
    <div class="alert alert-danger">
    <?php echo $wrong_password;?> 
    </div>    
    <?php }?>    
        <?php if(!empty($confirm)){?>    
    <div class="alert alert-danger">
    <?php echo $confirm;?> 
    </div>    
    <?php }?>    
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" value="">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- /.social-auth-links -->
      <br/>
    <a href="forgotpassword">I forgot my password</a><br>
    <a href="register" class="text-center">Register as  a new member</a>

  </div>
				</div>
				
				
				</div>	<!--/.row-->
			</div><!--section-->
		</div><!--/.container-->
    </section><!--/about-us-->
	
