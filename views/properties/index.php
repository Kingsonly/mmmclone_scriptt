<div class="dealers">
<div class="container">
	<h3>Dealers</h3>
	<div class="dealer">
		<h4>Find The Right Dealer For You</h4>
		<div class="dealer-grid">
					 			<?php foreach ($dealer as $key => $value) { ?>
			<div class="col-sm-4 dealer-grid1">

				<div class="dealer-grid-top">

					<span><img class="img-responsive" src="<?php echo URL.'public/'.'images/'.$value['pix']?>" alt=""></span>
					<h6><a href="single.html"><?php echo $value['name'] ?></a></h6>
					<div class="clearfix"> </div>
				</div>
				<p><?php echo $value['about'] ?></p>
					
			</div>	
			<?php } ?>
		</div>	
	</div>		

	<div class="dealer-top">
		<h4>Recent Deals</h4>

			<div class="deal-top-top">

					<?php foreach ($deal as $key => $value) { ?>
				<div class="col-md-3 top-deal-top">

					<div class=" top-deal">

						<a href="single.html" class="mask"><img style="height: height:250px;" class="img-responsive" src="<?php echo URL.'public/'.'images/'.$value['pix']?>" alt=""></a>
						<div class="deal-bottom">
							<div class="top-deal1">
								<h5><a href="views/properties/single"><?php echo $value['name'] ?></a></h5>
								<p><?php echo $value['details'] ?></p>
								<p><?php echo $value['price'] ?></p>
							</div>
							<div class="top-deal2">
								<a href="<?php echo index('properties/single'); ?>" class="hvr-sweep-to-right more">More</a>
							</div>
						<div class="clearfix"> </div>
						</div>	
					</div>	
				</div>
			<?php } ?>
			<div class="clearfix"> </div>
			
		</div>	
	</div>
</div>
</div>