<section id="services" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>Our Service</h2>
                <p class="lead">Select exclusive packages that fits you</p>
            </div>

            <div class="row">

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo URL; ?>public/images/services/services1.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Start Up</h3>
                            <p>N50,000/  24hr-5 Days</p>
                            <p>2:1 Matrix</p>
                            <p>Auto Assign</p>
                            
                            <p>Cash Out</p>
                            <p>Auto Recycle</p>
                            <p>N100,000 Return Investment</p>
                            <p><a href="./registration?recval=<?php echo md5('50') ?>"><button type="submit" class="btn btn-primary btn-block btn-flat">Sign Up</button></a></p>
                            
                        </div>
                    </div>

                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo URL; ?>public/images/services/services2.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Standard</h3>
                            <p>N100,000/  24hr-5 Days</p>
                            <p>2:1 Matrix</p>
                            <p>Auto Assign</p>
                            
                            <p>Cash Out</p>
                            <p>Auto Recycle</p>
                            <p>N200,000 Return Investment</p>
                            <p><a href="./registration?recval=<?php echo md5('100') ?>"><button type="submit" class="btn btn-primary btn-block btn-flat">Sign Up</button></a></p>
                            
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="<?php echo URL; ?>public/images/services/services3.png">
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Premium</h3>
                            <p>N200,000/  24hr-5 Days</p>
                            <p>2:1 Matrix</p>
                            <p>Auto Assign</p>
                            
                            <p>Cash Out</p>
                            <p>Auto Recycle</p>
                            <p>N400,000 Return Investment</p>
                            <p><a href="#"><button type="submit" class="btn btn-primary btn-block btn-flat">Coming Soon</button></a></p>
                            
                        </div>
                    </div>
                </div>  

                

                                                       
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#services-->