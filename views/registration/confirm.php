<style>
@import url(http://fonts.googleapis.com/css?family=Droid+Serif); /* to import google font style */ 
.content{
	width:960px;
	margin:20px auto;
	
	}
	
.main{
	float:left;
	width: 650px;
	margin-top:80px;
	
	}
#progressbar{
	margin:0;
	padding:0;
	font-size:18px;
	}	
.active{
	color:red;
	}
	
fieldset{
	display:none;
	
	padding:20px;
	margin-top:50px;
	margin-left: 85px;
	border-radius:5px;
	box-shadow: 3px 3px 25px 1px gray;
	}

#first{
    display:block;
	
	padding:20px;
	margin-top:50px;
	border-radius:5px;
	margin-left: 85px;
	box-shadow: 3px 3px 25px 1px gray;
	}
    
    p{
        color:#fff;
    }
	
input[type=text],
input[type=password],
select{
	width:100%;
	margin:10px 0;
	height:40px;
	padding:5px;
	border: 3px solid rgb(236, 176, 220);
	border-radius: 4px;
    color: #fff;
	}
	
textarea{
	width:100%;
	margin:10px 0;
	height:70px;
	padding:5px;
	border: 3px solid rgb(236, 176, 220);
	border-radius: 4px;
	}
	
input[type=submit],
input[type=button]{
	width: 120px;
	margin:15px 25px;
	padding: 5px;
	height: 40px;
	background-color: sienna;
	border: none;
	border-radius: 4px;
	color: white;
	font-family: 'Droid Serif', serif;
    
    margin-right: 4px;
	}
	
h2,p{
	text-align:center;
	font-family: 'Droid Serif', serif;
	}
	
li{
	margin-right:52px;
	display:inline;
	color:#c1c5cc;
	font-family: 'Droid Serif', serif;
	
	}
    
    
    
    @media screen and (max-width: 641px) {
        
        .content{
	width:auto;
	margin:0;
	
	}
	
.main{
	float:none;
	width:auto;
	margin-top:0;
	
	}
#progressbar{
	margin:0;
	padding:0;
	font-size:12px;
	}	
.active{
	color:red;
	}
	
fieldset{
	display:none;
	
	padding:20px;
	margin-top:0;
	margin-left:0;
	border-radius:5px;
	box-shadow: 3px 3px 25px 1px gray;
	}

#first{
    display:block;
	
	padding:20px;
	margin-top:0;
	border-radius:5px;
	margin-left:0;
	box-shadow: 3px 3px 25px 1px gray;
	}
    
    p{
        color:#fff;
    }
	
input[type=text],
input[type=password],
select{
	width:100%;
	margin:10px 0;
	height:40px;
	padding:5px;
	border: 3px solid rgb(236, 176, 220);
	border-radius: 4px;
    color:#fff;
	}
	
textarea{
	width:100%;
	margin:10px 0;
	height:70px;
	padding:5px;
	border: 3px solid rgb(236, 176, 220);
	border-radius: 4px;
    color: #fff;
	}
	
input[type=submit],
input[type=button]{
	width: 120px;
	margin:0;
	padding: 5px;
	height: 40px;
	background-color: sienna;
    display: inline-block;
	border: none;
	border-radius: 4px;
	color: white;
	font-family: 'Droid Serif', serif;
    
    margin-right:0;
	}
	
h2,p{
	text-align:center;
	font-family: 'Droid Serif', serif;
	}
	
li{
	margin-right:5px;
	display:inline;
	color:#c1c5cc;
	font-family: 'Droid Serif', serif;
	
	}
    
}
/*---------------------------------------------
	CSS For right side advertisement
-----------------------------------------------*/	
	
</style>
<section id="services" class="service-item">
	   <div class="container">
<?php if(!empty($details)){ ?>

  <div >
  <div style="color: #333;">
      
     <!-- //echo $details['first_name'].' '.$details['last_name'] .'an email have being sent to '.$details['email'];
      //echo '<br/> Your refferal link is'.$details['ref']; -->
      
      <div class="row">
<!-- multistep form -->
<div  class="main col-sm-12">
<form class="regform" action="<?php echo URL; ?>registration/insert_bank_details" method ="post">
	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">Bank Name</li>
		<li>Account Name</li>
		<li>Account Number</li>
	</ul>
	
	<!-- fieldsets -->
	<fieldset id="first">
		<h2 class="title">Bank Name</h2>
		<p class="subtitle">Please Make Sure Bank Name Is Correct</p>
		<input type="text" class="text_field" id="bnkName" name="" placeholder="Bank Name" /><br/>
		<input type="text" class="text_field" id="conbnkName" name="confirm_bank" placeholder="Confirm Bank Name" /><br/>
		
		<input type="button" name="next" class="next_btn" value="Next" />
	</fieldset>
	<fieldset>
		<h2 class="title">Account Name</h2>
		<p class="subtitle">Please Make Sure You Use You Correct Account Name IN Full</p>
		
		<input type="text" class="text_field" id="accName" name="" placeholder="Account Name" /><br/>
		<input type="text" class="text_field" id="conAccName" name="confirm_accname" placeholder="Confirm Account Name" /><br/>
		<input type="button" name="previous" class="pre_btn" value="Previous" />
		<input type="button" name="next" class="next_btn" value="Next" />
	</fieldset>
	<fieldset>
		<h2 class="title">Account Number</h2>
		<p class="subtitle">Note You can Not Change This Number Once Submited </p>
        <input type="hidden" value="<?php echo $details['ref']; ?>" name="ref"/>
		<input type="text" class="text_field" id="accNum" name="" placeholder="Account Number" /><br/>
		<input type="text" class="text_field" id="conAccNum" name="confirm_accnumber" placeholder="Confirm Account Number" /><br/>
		<input type="button" name="previous" class="pre_btn" value="Previous" />
		<input type="submit" class="submit_btn" value="Submit" />
	</fieldset>
</form>
</div>

<!----------------------------------------------------------------- 
Right side advertisement Div
----------------------------------------------------------------------->


</div>
      
      
      
     <?php }else{ ?>
          <?php echo $errors; ?>
   <?php   } ?>
      </div>
</div> 
    </div>
</section>
<!-- /form -->

