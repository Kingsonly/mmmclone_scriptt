    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo URL ?>public/js/jquery-2.2.3.min.js"></script>

    <script src="<?php echo URL ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo URL ?>public/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo URL ?>public/js/js/index.js"></script>
    <script src="<?php echo URL ?>public/js/jquery.isotope.min.js"></script>   
    <script src="<?php echo URL ?>public/js/wow.min.js"></script>
	<script src="<?php echo URL ?>public/js/main.js"></script>
    <script src="<?php echo URL ?>public/js/multi_step_form.js"></script>


</body>

</html>