

  <!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo URL; ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo URL; ?>public/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo URL; ?>public/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo URL; ?>public/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo URL; ?>public/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<script src="<?php echo URL; ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo URL; ?>public/lugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo URL; ?>public/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo URL; ?>public/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo URL; ?>public/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo URL; ?>public/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo URL; ?>public/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo URL; ?>public/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo URL; ?>public/dist/js/app.min.js"></script>
<script src="<?php echo URL; ?>public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo URL; ?>public/dist/js/demo.js"></script>

<script src="<?php echo URL; ?>public/js/register_index.js"></script>
<script src="<?php echo URL; ?>public/angular/angular.js"></script>
<script src="<?php echo URL; ?>public/angular/angular-route.js"></script>
<script src="<?php echo URL; ?>public/js/app.js"></script>
<script src="<?php echo URL; ?>public/js/dirPagination.js"></script>
<script src="<?php echo URL ?>public/js/multi_step_form.js"></script>
 




<script>
    
    $('.demo12').countdown('<?php echo $v['end_time'] ?>', function(event) {
                                    $(this).html(event.strftime('%w weeks %d days %H:%M:%S'));
                                  });
  
                                  
    
    
    $("#uploadimage").on('submit',(function(e) {
e.preventDefault();
$("#message").empty();
$('#loading').show();
$.ajax({
url: "ajax_php_file.php", // Url to which the request is send
type: "POST",             // Type of request to be send, called as method
data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
contentType: false,       // The content type used when sending data to the server.
cache: false,             // To unable request pages to be cached
processData:false,        // To send DOMDocument or non processed data file it is set to false
success: function(data)   // A function to be called if request succeeds
{
$('#loading').hide();
$("#message").html(data);
}
});
}));

// Function to preview image after validation
$(function() {
$("#file").change(function() {
$("#message").empty(); // To remove the previous error message
var file = this.files[0];
var imagefile = file.type;
var match= ["image/jpeg","image/png","image/jpg"];
if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
{
$('#previewing').attr('src','noimage.png');
$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
return false;
}
else
{
var reader = new FileReader();
reader.onload = imageIsLoaded;
reader.readAsDataURL(this.files[0]);
}
});
});
function imageIsLoaded(e) {
$("#file").css("color","green");
$('#image_preview').css("display", "block");
$('#previewing').attr('src', e.target.result);
$('#previewing').attr('width', '250px');
$('#previewing').attr('height', '230px');
};
    
    
    
    
    $(function () {
    //Add text editor
    $("#compose-textarea").wysihtml5();
  });
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
     
</script>

<script>
    ///// send complain
    
            $("#sendcomplain").click(function(event)
  {
    //event.preventDefault(); // cancel default behavior
  var $myForm = $('#complain')
if ($myForm[0].checkValidity()) {
    $('.loader').show();
  // If the form is invalid, submit it. The form won't actually submit;
  // this will just cause the browser to display the native HTML5 error messages.
  event.preventDefault();
    $.ajax({
		url: '<?php echo URL.'dashboard/runcomplain'; ?>',
        type: 'post',
		data: $('input, textarea'),
		dataType: 'html',
		success: function(html) {
            
			$('.loader').html('message sent');
			//$('#checkout .checkout-heading').addClass('active');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert('sorry');
		}
	});
    
}
   else{
       //$myForm.find(':submit').click()
   }

    //... rest of add logic
  });
    
    $("#xsendcomplain").click(function(event)
  {
    //event.preventDefault(); // cancel default behavior
  var $myForm = $('#xcomplain')
if ($myForm[0].checkValidity()) {
     $('.loader').show();
  // If the form is invalid, submit it. The form won't actually submit;
  // this will just cause the browser to display the native HTML5 error messages.
  event.preventDefault();
    $.ajax({
		url: '<?php echo URL.'dashboard/runcomplain'; ?>',
        type: 'post',
		data: $('input, textarea'),
		dataType: 'html',
		success: function(html) {
            
			$('.loader').html('message sent');
			//$('#checkout .checkout-heading').addClass('active');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert('sorry');
		}
	});
    
}
   else{
       //$myForm.find(':submit').click()
   }

    //... rest of add logic
  });
    
    
    
    $("#sendtestimony").click(function(event)
  {
    //event.preventDefault(); // cancel default behavior
  var $myForm = $('#testimony')
if ($myForm[0].checkValidity()) {
     $('.loader').show();
  // If the form is invalid, submit it. The form won't actually submit;
  // this will just cause the browser to display the native HTML5 error messages.
  event.preventDefault();
    $.ajax({
		url: '<?php echo URL.'dashboard/runtestimonials'; ?>',
        type: 'post',
		data: $('input, textarea'),
		dataType: 'html',
		success: function(html) {
            
			$('.loader').html('message sent');
			//$('#checkout .checkout-heading').addClass('active');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert('sorry');
		}
	});
    
}
   else{
       //$myForm.find(':submit').click()
   }

    //... rest of add logic
  });
    
    
    
    
     
        
        $("#AddItem").click(function(event)
  {
    //event.preventDefault(); // cancel default behavior
  var $myForm = $('#sendsmsclient')
if ($myForm[0].checkValidity()) {
  // If the form is invalid, submit it. The form won't actually submit;
  // this will just cause the browser to display the native HTML5 error messages.
  event.preventDefault();
    $.ajax({
		url: '<?php echo URL.'dashboard/smssend'; ?>',
        type: 'post',
		data: $('input, textarea'),
		dataType: 'html',
		success: function(html) {
            alert(html);
			$('#messagesent').html(html);
			//$('#checkout .checkout-heading').addClass('active');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert('sorry');
		}
	});
    
}
   else{
       $myForm.find(':submit').click()
   }

    //... rest of add logic
  });
    $("#example1").DataTable();
    $('#example2 , #example3 , #example4 , #example5 , #example6').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
        "pageLength": 5
    });
    
    
    
    
    
    $("#uploadForm").on('submit',(function(e) {
		e.preventDefault();
        var id=$('.inputFile').val();
        if(id==''){
            alert(1234);
            return false;
        }
		$.ajax({
        	url: "<?php echo URL.'dashboard/profile_run?upload'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
			$("#targetLayer").html(data);
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
    
    
    //ajax for address
    $("#address").on('submit',(function(e) {
		e.preventDefault();
        var id=$('#addresss').val();
        if(id==''){
            alert(1234);
            return false;
        }
		$.ajax({
        	url: "<?php echo URL.'dashboard/profile_run?newadress'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
			$("#tagetadress").html(data);
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
    
    //ajax for password
    $("#updatepwd").on('submit',(function(e) {
		e.preventDefault();
        var id=$('#pwd').val();
        if(id==''){
            //alert(1234);
            $("#tagetpwd").html('sorry password cant be empty');
            return false;
        }else{
		$.ajax({
        	url: "<?php echo URL.'dashboard/profile_run?newpassword'; ?>",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
			$("#tagetpwd").html(data);
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });}
	}));
    
    
    
    
    
    
</script>




</body>
</html>

