 <section id="portfolio">
        <div class="container">
            <div class="center">
               <h2>Testimonials</h2>
               <p class="lead">See how people from the different packages have benefitted from this awsome opportunity.</p>
            </div>
        

            <ul class="portfolio-filter text-center">
                <li><a class="btn btn-primary active" href="#" data-filter="*">ALL</a></li>
                <li><a class="btn btn-primary" href="#" data-filter=".bootstrap">BASIC</a></li>
                <li><a class="btn btn-primary" href="#" data-filter=".html">STANDARD</a></li>
                <li><a class="btn btn-primary" href="#" data-filter=".wordpress">PREMIUM</a></li>
            </ul><!--/#portfolio-filter-->

            <div class="row">
                <div class="portfolio-items">
                    <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item1.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">Mr L. T Akinde</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p>
                                    <a class="preview" href="public/images/portfolio/full/item1.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item joomla bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item2.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">Annonymous</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p></p>
                                    <a class="preview" href="public/images/portfolio/full/item2.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>          
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item bootstrap wordpress col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item3.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                                                       <h3><a href="#">Mr L. T Akinde</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p>
                                    <a class="preview" href="public/images/portfolio/full/item3.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>        
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item joomla wordpress apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item4.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                                                       <h3><a href="#">Mr L. T Akinde</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p>
                                    <a class="preview" href="public/images/portfolio/full/item4.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>           
                    </div><!--/.portfolio-item-->
          
                    <div class="portfolio-item joomla html bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item5.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                                                       <h3><a href="#">Mr L. T Akinde</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p>
                                    <a class="preview" href="public/images/portfolio/full/item5.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>      
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item wordpress html apps col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item6.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                                                       <h3><a href="#">Mr L. T Akinde</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p>
                                    <a class="preview" href="public/images/portfolio/full/item6.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>         
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item wordpress html col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item7.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                                                       <h3><a href="#">Mr L. T Akinde</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p>
                                    <a class="preview" href="public/images/portfolio/full/item7.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>          
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item wordpress html bootstrap col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="public/images/portfolio/recent/item8.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                                                        <h3><a href="#">Mr L. T Akinde</a></h3>
                                    <p>"This is the best business venture Ive been involved in!"</p>
                                    <a class="preview" href="public/images/portfolio/full/item8.png" rel="prettyPhoto"><i class="fa fa-eye"></i> View</a>
                                </div> 
                            </div>
                        </div>          
                    </div><!--/.portfolio-item-->
                </div>
            </div>
                      <div class="clients-area center wow fadeInDown">
                <h2>From our Highest Investors</h2>
            </div>

            <div class="row">
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="public/images/client1.png" class="img-circle" alt="">
                        <h3>"This is the best business venture Ive been involved in!"</h3>
                        <h4><span>-Yusuf Tanko /</span>  Premium Account - Master</h4>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="public/images/client2.png" class="img-circle" alt="">
                        <h3>"This is the best business venture Ive been involved in!"</h3>
                        <h4><span>-Bolarin O. Taiwo /</span>  Premium Account - Master</h4>
                    </div>
                </div>
                <div class="col-md-4 wow fadeInDown">
                    <div class="clients-comments text-center">
                        <img src="public/images/client3.png" class="img-circle" alt="">
                        <h3>"This is the best business venture Ive been involved in!"</h3>
                        <h4><span>-Emediong Isoken /</span> Premium Account - Master</h4>
                    </div>
                </div>
           </div>
        </div>
    </section><!--/#portfolio-item-->